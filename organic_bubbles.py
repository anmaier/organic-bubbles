# This project creates an animation showing bubbles moving, merging and spliting in an organic way

"""The grid is made of points separated by a distance dx in the x-direction and dy in the y-direction, 
and there are Nx points in the x-direction and Ny points in the y-direction."""

import numpy as np
from PIL import Image, ImageDraw


class Bubble:
    """A Bubble is a cone centered in self.position, with base radius self.radius."""

    def __init__(self, position, radius, velocity) -> None:
        self.position = position
        self.radius = radius
        self.velocity = velocity

    def evaluate(self, x, y):
        """Evaluates the height of the bubble at (x, y)."""
        r = np.linalg.norm(np.array((x, y), dtype=np.float) - self.position)
        result = 0.0
        if r < self.radius:
            result = 1 - r / self.radius
        return result


class Board:
    """The board is the the playground. It countains the bubbles, and the borders.
    It is responsible of the spatial evolution of the bubbles."""

    def __init__(self):
        self.size = (100, 100)
        self.bubbles = []
        self.dt = 0.3
        self.graphical_interface = Image.new('RGB', self.size)
        self.animation_images = []

        # Add some bubbles
        self.add_bubbles_random(5)

    def add_bubbles_random(self, number_of_bubbles):
        """Adds a number equals to number_of_bubbles of bubbles where their position, widths and velocity are generated randomly."""

        for _ in range(number_of_bubbles):
            random_position = np.random.uniform(
                low=0.0, high=self.size, size=2)
            random_velocity = np.random.normal(loc=0.0, scale=10.0, size=2)
            self.bubbles.append(
                Bubble(random_position, 20.0, random_velocity))

    def step(self):
        """Updates the position of the bubbles after 1 step."""
        for bubble in self.bubbles:
            bubble.position += bubble.velocity * self.dt
            # Check collision with a border.
            # If collision, set position to be inside the board and invert
            # the velocity in the corresponding dimension
            if bubble.position[0] <= 0.0:  # Left border collision
                bubble.position[0] = -bubble.position[0]
                bubble.velocity[0] *= -1
            if bubble.position[0] >= self.size[0]:  # Right border collision
                bubble.position[0] = self.size[0] - \
                    (bubble.position[0] - self.size[0])
                bubble.velocity[0] *= -1
            if bubble.position[1] <= 0.0:  # Bottom border collision
                bubble.position[1] = -bubble.position[1]
                bubble.velocity[1] *= -1
            if bubble.position[1] >= self.size[1]:  # Top border collision
                bubble.position[1] = self.size[1] - \
                    (bubble.position[1] - self.size[1])
                bubble.velocity[1] *= -1

    def draw(self):
        """Draws the bubbles and add the image in self.animation_images."""
        image = Image.new('RGB', self.size)
        image_draw = ImageDraw.Draw(image)
        """for bubble in self.bubbles:
            image_draw.point(tuple(bubble.position), (255, 255, 255))"""
        for x in range(self.size[0]):
            for y in range(self.size[1]):
                if np.abs(self._bubble_mixture(x, y) - 0.5) < 0.05:
                    image_draw.point((x, y), (255, 255, 255))

        self.animation_images.append(image)

    def run(self):
        """Main loop of the class, at each iteration, updates the position of the bubbles and draws the frame."""
        for i in range(100):
            self.step()
            self.draw()
        self.animation_images[0].save(
            'out.gif', save_all=True, append_images=self.animation_images[1:], optimize=True, loop=0)

    def _bubble_mixture(self, x, y):
        """Evaluates the bubble mixture at (x, y)."""
        sum = 0
        for bubble in self.bubbles:
            sum += bubble.evaluate(x, y)
        return sum


my_board = Board()
my_board.run()

# TODO Clarify the sizes, the length scale, etc. Maybe separate the pixels division and the physical lengths
# TODO Instead of modifying each pixel separately (very slow), create a ndarray where each cell is a pixel
# and evaluate it with a condition applied to each element
# TODO Doc
# TODO Add linter (pylint for example)
# TODO Add profiler
# TODO Add Numba or something similar to accelerate the code
